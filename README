This is the python interface for the MIA library. It uses numpy to 
represent images, and currently, filters and image registration is 
supported. 

  Compiling the interface:

The build system utilizes python distutils. To compile the interface 
the compiler must support the -std=c++11 compiler flag. 
Building the interface then build down to running 

  python setup.py build 

Note, that you wil need mia >= 2.0.10 to be installed. 


  Using the python interface: 

Given that MyImage is a 2D or 3D numpy array of scalar values, one may 
run a filter chain like 
 
  import mia

  MyFilteredImage = mia.filter(MyImage, ["filter1:param1=a", 
                                     "filter2:param1=b,param2=c", ...])

For the availabe filters see the user reference of MIA.


Similarly, the image registration can be run with images Moving and Reference, 
using a spline based transformation with a coefficient rate of 5 pixels, 2 
multi-resolution levels and an nlopt based optimizer: 

  import mia
  Registered = mia.register_images(src=Moving, ref=Reference, 
     transform="spline:rate=5", cost=["image:cost=ssd", "divcurl:weight=10.0"],
     mglevels=2, optimizer="nlopt:opt=ld-var1,xtola=0.001,ftolr=0.001,maxiter=300")


The result image is stored in Registered. Currently, the transformation is lost.
