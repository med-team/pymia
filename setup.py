# This file is part of pymia - python bindings for MIA
# Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
#
# pymia is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pymia; if not, see <http://www.gnu.org/licenses/>.
#

from distutils.core import setup
from distutils.extension import Extension

import numpy
import subprocess

#
# as seen on 
# http://code.activestate.com/recipes/502261-python-distutils-pkg-config/


def uniq(input):
    output = []
    for x in input:
        if x not in output:
            output.append(x)
    return output


def pkgconfig(*packages, **kw):
    flag_map = {'-I': 'include_dirs', '-L': 'library_dirs', '-l': 'libraries'}

    s = subprocess.check_output(['pkg-config','--libs', '--cflags', ' '.join(packages)]).decode('utf-8')
    for token in s.split():
    
        if token[:2] in flag_map:
            kw.setdefault(flag_map.get(token[:2]), []).append(token[2:])
        else: # throw others to extra_link_args
            kw.setdefault('extra_link_args', []).append(token)

    for k, v in kw.items(): # remove duplicated
        kw[k] = uniq(v)

    return kw

depvars = pkgconfig('mia3d-2.4')
print(depvars)
depvars['include_dirs'].append(numpy.get_include())


extension = Extension('pymia', 
                      ['src/mia_python.cc'], 
                      extra_compile_args = ['-std=c++11'], 
                      depends = ["numpy"], 
                      **depvars)


            
setup(name='pymia',
      version='0.1.9',
      description='Functions for mediacal image analysis ',
      long_description='This package provides a module for gray scale image processing and a tool for basic image segmentation' , 
      author='Gert Wollny',
      author_email='gw.fossdev@gmail.com',
      license = "GNU General Public License", 
      platforms = ["Linux", "BSD"], 
      url='http://mia.sourceforge.net/',
      ext_modules = [extension], 
      classifiers=[
          'Development Status :: 4 - Beta',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU General Public License',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Science :: Image Processing',
          ],
      )





